# Administrative Documents

> ## [Juncos shop policies and rules](https://docs.google.com/document/d/15V0Ad79sJq_QXlJ7iKV9vGBSLVEcTkorV2piE-3NtcU/edit?usp=sharing)
>
> ## [Track day sign-up (sheet 1) & Juncos shop usage sign-up (sheet 2)](https://docs.google.com/spreadsheets/d/1gx1NOclwzNJXCJ1Yo-FYE7FBAqmiTWybTRlOT8_J4ec/edit?usp=sharing)
>
> ## [Weekly Testing Reports](https://gitlab.com/IACBaseSoftware/testingorga/-/tree/master/Testing%20Reports)
>
> ## [Current Gitlab Issues](https://gitlab.com/IACBaseSoftware/testingorga/-/issues)
> Track current issues, milestones, testing plans, responsible teams, etc.
> Central location to track ongoing hardware/software issues and create testing plans.
>
> ## [K&K Waiver Submission List](https://docs.google.com/spreadsheets/d/1DLGovWho6KROah-zFDIKOXG46NMvV8SvIuFinBPBLE4/edit#gid=0)
>
> ## [IMS Testing Plan 8-23](https://docs.google.com/spreadsheets/d/1ggOgjklB9hVS2tYKQWFQay-9CaErumNuqXiFUemtNXg/edit?usp=sharing)

# [Root BVS Drive](https://drive.google.com/drive/u/0/folders/1YD2HWMUGwuVgdbVl2gt0GFJEWLvwayri)
The root BVS drive holds notes / documentation / other collaborative documents that might need to be edited live collaboratively.

> ## [Vehicle Operation Documents](https://drive.google.com/drive/folders/1JVG8Rn8JUFnMwqSf3oCGC63PNTQTugUH)
>> ### [Vehicle Launch Procedure](https://docs.google.com/document/d/1UIEyh4TvBzEQMtyYA5MZqf5uN-MHqRuI)
>> Vehicle startup, shutdown, battery charging, and manual shifting procedures, etc.
>>
>> ### [Basestation Launch Procedure](https://drive.google.com/file/d/1ROqRwYaM39N6PULXTBjaiT3Y9Xkl0rYv)
>> Launching and running the basestation.
>>
>> ### [Common Issues and Troubleshooting Procedures](https://docs.google.com/document/d/1Z-e8U4bES0nnpF0WBzlROQLCEe4g8HPZZvsjrsJg2ik/edit?usp=sharing)
>> Common issues, trobleshooting guide, manual gear change procedure, and additional notes for operation
>>
>> ### [System / CT State Docs](https://docs.google.com/document/d/1uumCjwCSKZlu29cnbPkLVVDrKig7J63UO0SPkvDedE0)
>> What different vehicle states mean what and a few trouble shooting steps in the notes.
>>
>> ### [Powertrain Documentation](https://docs.google.com/document/d/1gJYag8tVhtnSl3MjWvsVMBEfaR9WXndrybvJtXeLbU8/edit?usp=sharing)
>> It contains general documentation about the engine, transmission, shifting and Motec.
>>
>> ### [Space Drive Documentation](https://drive.google.com/drive/folders/1akBgl3Wvft2Gq8eYYKsgkLd4604RLnFg)
>> It contains the documentation about the space drive system.
>>
>> ### [Vehicle Setup/Configuration](https://drive.google.com/drive/folders/115Ha91h5w9LPpzR-mv845ptqETM18HTs?usp=sharing)
>> Documentation about configuring the computer, Cisco switch, sensors, and IPs.
>>
>> ### [Joystick Mapping](https://docs.google.com/document/d/1WWYNAFFzpRII0pFThqQvPx2t8Rw0z1KlxLhdZPmvpcA)
>>
>>### [ROS2 Topics List on ADLINK](https://docs.google.com/spreadsheets/d/1mMak6NYpLYeMDyXgDoRv21KneMGB-2ZgfGSiSLrMMVk/edit?usp=sharing)
>
>
> ## [Testing Documentation](https://drive.google.com/drive/u/2/folders/1kt0HqHxbO2fXAGNlumvO_llj789wwOU6)
> The test cards and other testing related information can be found [here](https://drive.google.com/drive/u/2/folders/1kt0HqHxbO2fXAGNlumvO_llj789wwOU6).
>
> ## [Videos / Media](https://drive.google.com/drive/folders/1t0PqBtlXrYfKF46WyI2GKyRNj0d0jjsw)
> A good place to put photos or videos that will need to be shared among the teams or are used in documentation.
>
> ## [Testing Supplies](https://docs.google.com/spreadsheets/d/1pr3UgcH081cYG0PtsuSb4UeVJQCuuYQA-2ILy6OUkyw/edit?usp=sharing)
> Testing supplies each team should purchase / procure for testing at Indy

# Software Development Guidelines
> ## Main points
>> - Create a branch for related changes.
>> - Commit often.
>> - Submit merge requests with 2 reviewers.
>> - Move towards CI (Continuous Integration)
> ## Useful Resources
>> ### [Software Development Best Practices Guidelines](https://www.dialexa.com/our-insights/2019/12/9/five-software-development-best-practices)
>> Nice guide for what to keep in mind when developing software. Short quick read.
>> ### [Code Review Best Practices](https://blog.palantir.com/code-review-best-practices-19e02780015f)
>> Good article on motivation for doing code reviews and best practices.
>> ### [GitLab Beginners Guide for Continuous Integration](https://about.gitlab.com/blog/2018/01/22/a-beginners-guide-to-continuous-integration)
>> Good place to start when looking into adding continuous integration to your GitLab repo.

# Vehicle Information

> ## [DO Master Vehicle Spreadsheet](https://gitlab.com/IACBaseSoftware/documentation/-/blob/main/DO_vehicle_info/IAC_DO12_Vehicle_Specification_V4.0_Release.xlsx)
>
> ## [DO Vehicle Architecture Overview](https://gitlab.com/IACBaseSoftware/documentation/-/blob/main/DO_vehicle_info/ArchitectureOverviewCompetitionTeams_20210225.pdf)
>
> ## [DO Final Presentation](https://gitlab.com/IACBaseSoftware/documentation/-/blob/main/DO_vehicle_info/DO12_CT-Documentation.pdf)
>
> ## [DO State Machine](https://gitlab.com/IACBaseSoftware/documentation/-/blob/main/DO_vehicle_info/DO12_Integrated_SM.pdf)

# Information on how to get you started on working with the car
- Basic ROS2 tutorials are a prerequiste for operating the car (https://docs.ros.org/en/foxy/Tutorials.html). Make sure you know about nodes, topics, launch files and how to build your own ros2 workspace.
- Basic understanding of git is a prerequisite for opreating the car. Make sure you know about branches, checkouts, push/pull, commits. You will not be able to use the software otherwise. There are loads of tutorials out there on the web, just pointing out something for your convenience: https://git-scm.com/docs/gittutorial
- Basic understanding of docker is helpful for debugging and required building/modifying the code on the car (https://docs.docker.com/get-started/)
- Basic understanding of how to build the Autoware workspace is required for modifying the code on the car (https://autowarefoundation.gitlab.io/autoware.auto/AutowareAuto/building.html)
- A good starting point on the architecture of the code is the iac_launch package to be found in src/tools/iac_launch in the AutowareAuto fork in the BVS Gitlab group.
