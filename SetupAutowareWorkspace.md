# Setup of Autoware workspace on your computer
This tutorial gives a short introduction on how to setup and build the Autoware fork for the IAC racing vehicle on your own machine.

## Setup your machine
You have to install docker and ADE, a tool which helps you manage the build environment for Autoware. Details on this process are documented in the [Autoware Documentation](https://autowarefoundation.gitlab.io/autoware.auto/AutowareAuto/installation-ade.html).

## Setup the repository
Go to your ade home directory and clone the IAC Autoware fork with the required branch *deeporange*.

```
cd ~/adehome
git clone https://gitlab.com/IACBaseSoftware/AutowareAuto.git
git checkout -t origin/deeporange
```

## Build the environment
Launch the ade environment and start the build via colcon
```
ade --rc .aderc-amd64-foxy start --update --enter
cd AutowareAuto
vcs --worker=1 import < autoware.auto.$ROS_DISTRO.repos
colcon build
```

## Install dependencies
If you want to run and build outside the ade container, make sure that you install all the dependencies using rosdep using the commands below from the Autoware folder
```
sudo apt-get update
sudo apt-get install python3-rosdep2 
rosdep update
rosdep install -y --from-paths src --ignore-src --rosdistro $ROS_DISTRO
```

## Build vimba camera (needs to be fixed)
```
# from outside the ade container
rm -rf build/avt_vimba_camera/ install/avt_vimba_camera/
colcon build --symlink-install --packages-select avt_vimba_camera
```
=======
## Building the ade container manually
Since we added private dependencies, the automatic build for the ade container fails. The following commands allow you to build it manually (run from Autoware repository). Make sure the rosdep command is run from a fresh ros:foxy container, otherwise the dependencies might not match. 
```
vcs  --worker=1 import < autoware.auto.$ROS_DISTRO.repos
docker run -it -v ~/adehome/AutowareAuto:/home ros:foxy
cd /home
rosdep update
rosdep install --as-root "apt:false pip:false" --simulate --reinstall --ignore-src -y --from-paths src | sort >> ros-deps
exit
mv ros-deps tools/ade_image
cp autoware.auto.foxy.repos tools/ade_image
cd tools/ade_image
docker build --build-arg ROS_DISTRO=${ROS_DISTRO} --build-arg CODENAME=focal -t registry.gitlab.com/iacbasesoftware/autowareauto/amd64/ade-foxy:iac-dev .
```
